const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;

const server = require('./../app');
const Template = require('./../db/models').Template;
const {testTemplates, populateTemplates} = require('./../db/seeders/seeders');

chai.use(chaiHttp);

beforeEach(populateTemplates);

describe('POST /campaigns/template', () => {
  it('should create new template', (done) => {
    chai.request(server)
      .post('/campaigns/template')
      .send(testTemplates[0])
      .end((err, res) => {
        if (err) return done(err);

        expect(res).to.have.status(200);
        expect(res.body).to.include(testTemplates[0]);
        Template.findAll()
          .then((templates) => {
            expect(templates).to.have.lengthOf(3)
            done()
          })
          .catch((e) => {
            done(e);
          });
      });
  });

  it(('Should not create template that violates validation'), (done) => {
    const name = 'te';
    const mailTemplate = 'ja';

    chai.request(server)
      .post('/campaigns/template')
      .send({
        name: name,
        templateType: testTemplates[0].templateType,
        mailTemplate: testTemplates[0].mailTemplate
      })
      .end((err, res) => {
        // if (err) return done(err);
        expect(res).to.have.status(400);
        expect(res.body).to.include({name: 'SequelizeValidationError'});
        done();
      })
  });

  it(('Should not create template with invalid body data'), (done) => {
    const name = '   a ';

    chai.request(server)
      .post('/campaigns/template')
      .send({
        name: name,
        templateType: testTemplates[0].templateType,
        mailTemplate: testTemplates[0].mailTemplate
      })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body).to.include({ name: 'SequelizeValidationError' });
        done();
      })
  });
});