$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})


// Sidebar toggler
$(function () {
  let $theEl = $('a#campaignMenu .fa');
  let clickDisabled = false;
  let firstTime = true;
  let curUrl = window.location.href;

  $('#campaignMenu').click(function () {
    if (curUrl.indexOf('/campaigns/') >= 0 || clickDisabled) return;

    if (firstTime) {
      $theEl.removeClass('fa-plus');
      $theEl.addClass('fa-minus');
      firstTime = false;
      clickDisabled = true;
    } else {
      if ($('#campaignSubmenu a.nav-link').is(':visible')) {
        if ($('.fa-plus')[0]) {
          $theEl.removeClass('fa-plus');
          $theEl.addClass('fa-minus');
          clickDisabled = true
        } else {
          $theEl.removeClass('fa-minus');
          $theEl.addClass('fa-plus');
          clickDisabled = true
        }
      } else {
        if ($('.fa-plus')[0]) {
          $theEl.removeClass('fa-plus');
          $theEl.addClass('fa-minus');
          clickDisabled = true;
        } else {
          $theEl.removeClass('fa-minus');
          $theEl.addClass('fa-plus');
          clickDisabled = true;
        }
      }
    }
    setTimeout(function () { clickDisabled = false; }, 350);
  });
});