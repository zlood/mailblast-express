$(function () {
  function toJSONString(form) {
    var obj = {};
    var elements = form.querySelectorAll('input[id=templateNameInput], textarea, input[name=templateType]:checked');
    
    for (var i = 0; i < elements.length; ++i) {
      var element = elements[i];
      var name = element.name;
      var value = element.value;

      if (name) {
        obj[name] = value;
      }
    }
    return JSON.stringify(obj);
  }

  $(document).ready(function () {
    $('#templateForm').submit(function (ev) {
      ev.preventDefault();
      const json = toJSONString(this);

      $.ajax({
        type: 'POST',
        url: '/campaigns/template',
        data: json,
        dataType: 'json',
        contentType: 'application/json'
      })
      .done((result) => {
        window.location = "/campaigns/template";
      })
      .fail((jqXHR, textStatus) => {
        let arr = jqXHR.responseJSON.errors;
        let arrLen = arr.length;
        let res = '';
        for (i = 0; i < arrLen; i++) {
          res += `<p style=\"color: red\">${arr[i].value} is not a valid value of ${arr[i].path}</p>`
        }
        $('#errorModal').html(res);
      });
    });
  });
});

$('#modalCloseBtn').click(function () {
  $('#errorModal').text('');
});

$('.tool-btn').click(function (ev) {
  const id = $(ev.target).closest('tr').attr('id');
  $('#removeTemplateConfirmButton').click(function (event) {
    event.preventDefault();
    $.ajax({
      type: 'DELETE',
      url: `/campaigns/template/${id}`
    })
    .done((result) => {
      window.location = "/campaigns/template";
    })
    .fail((jqXHR, textStatus) => {
      const res = `${jqXHR}`;
      $('#errorModal').html(res);
    });
  });
});