$(function () {
  var chart;
  var month = 12;
  var year = 2017;

  $(document).ready(function () {


    // usage chart
    $.ajax({
      type: "GET",
      url: `/usage/${year}/${month}`
    }).done(res => {
      var usageChart = new Highcharts.chart('usageChart', {
        chart: {
          type: 'gauge',
          plotBackgroundColor: null,
          plotBackgroundImage: null,
          plotBorderWidth: 0,
          plotShadow: false
        },

        title: {
          text: 'Mail Usage'
        },

        pane: {
          startAngle: -150,
          endAngle: 150,
          background: [{
            backgroundColor: {
              linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
              stops: [
                [0, '#FFF'],
                [1, '#333']
              ]
            },
            borderWidth: 0,
            outerRadius: '109%'
          }, {
            backgroundColor: {
              linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
              stops: [
                [0, '#333'],
                [1, '#FFF']
              ]
            },
            borderWidth: 1,
            outerRadius: '107%'
          }, {
            // default background
          }, {
            backgroundColor: '#DDD',
            borderWidth: 0,
            outerRadius: '105%',
            innerRadius: '103%'
          }]
        },

        // the value axis
        yAxis: {
          min: 0,
          max: 13000,

          minorTickInterval: 'auto',
          minorTickWidth: 1,
          minorTickLength: 10,
          minorTickPosition: 'inside',
          minorTickColor: '#666',

          tickPixelInterval: 30,
          tickWidth: 2,
          tickPosition: 'inside',
          tickLength: 10,
          tickColor: '#666',
          labels: {
            step: 2,
            rotation: 'auto'
          },
          title: {
            text: 'mails sent'
          },
          plotBands: [{
            from: 0,
            to: 8000,
            color: '#55BF3B' // green
          }, {
            from: 8000,
            to: 10000,
            color: '#DDDF0D' // yellow
          }, {
            from: 10000,
            to: 13000,
            color: '#DF5353' // red
          }]
        },

        series: [{
          name: 'mails',
          data: [res.usage[0].usage],
          tooltip: {
            valueSuffix: ' mails'
          }
        }]

      })
    });


    // delivery chart
    $.ajax({
      type: "GET",
      url: `/delivery/${year}/${month}`
    }).done(res => {
      var deliveryChart = new Highcharts.chart('deliveryChart', {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
        },
        title: {
          text: 'Delivery Report'
        },
        tooltip: {
          pointFormat: '{series.name}: <b>{point.y}</b> ({point.percentage:.1f}%)'
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            colors: ['#55BF3B', '#DF5353', '#DDDF0D'],
            cursor: 'pointer',
            dataLabels: {
              enabled: false
            },
            showInLegend: true
          }
        },
        series: [{
          name: 'Total',
          colorByPoint: true,
          data: [{
            name: 'Success',
            y: res.delivery[0].delivery.success
          }, {
            name: 'Failed',
            y: res.delivery[0].delivery.failed
          }, {
            name: 'Unkown',
            y: res.delivery[0].delivery.unknown
          }]
        }]
      });
    })
  })

});