'use strict';
module.exports = (sequelize, DataTypes) => {
  var Template = sequelize.define('Template', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [3, 30]
      }
    },
    mailTemplate: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        len: [6]
      }
    },
    templateType: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return Template;
};