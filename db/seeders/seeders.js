const Template = require('./../models').Template;

const testTemplates = [{
  name: 'test 1',
  templateType: 'HTML',
  mailTemplate: 'this is a test template only'
},{
  name: 'test 2',
  templateType: 'rawText',
  mailTemplate: 'this is another test template'
}];

const populateTemplates = (done) => {
  Template.destroy({
    where: {}
  })
    .then((deletedRow) => {
      Template.bulkCreate(testTemplates);
    })
    .then(() => done());
};

module.exports = {testTemplates, populateTemplates};
