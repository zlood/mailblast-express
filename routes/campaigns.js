const express = require('express');
const router = express.Router();

const collections = require('./../controllers/collections');
const templates = require('./../controllers/templates');
const lists = require('./../controllers/lists');

// List API
router.route('/list')
  .post()
  .get(lists.getAllList)

router.route('/list/:id')
  .get()
  .delete();

// Collections API
router.route('/collection')
  .post()
  .get();

router.route('/collection/:id')
  .get()
  .delete()
  .patch();

// Template API
router.route('/template')
  .post(templates.createTemplate)
  .get(templates.getAllTemplate);

router.route('/template/:id')
  .get()
  .delete(templates.deleteTemplateByID)
  .patch();

module.exports = router;
