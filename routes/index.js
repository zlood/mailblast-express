var express = require('express');
var router = express.Router();
var _ = require('lodash')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Dashboard', isHomePage: true });
});

router.get('/usage/:year/:month', function (req, res, next) {

  var year = req.params.year
  var month = req.params.month

  // temporary mock data
  var usageList = [{
    year: 2018,
    month: 1,
    usage: 7605
  }, {
    year: 2017,
    month: 12,
    usage: 11321
  }, {
    year: 2017,
    month: 11,
    usage: 10874
  }]

  var usage = usageList.filter(item => {
    return item.year == year && item.month == month
  })

  res.status(200)
    .send({ usage })
})

router.get('/delivery/:year/:month', function (req, res, next) {

  var year = req.params.year
  var month = req.params.month

  // temporary mock data
  var deliveryList = [{
    year: 2018,
    month: 1,
    delivery: {
      success: 7109,
      failed: 496,
      unknown: 0
    }
  }, {
    year: 2017,
    month: 12,
    delivery: {
      success: 10089,
      failed: 1188,
      unknown: 144
    }
  }, {
    year: 2017,
    month: 11,
    delivery: {
      success: 9844,
      failed: 1029,
      unknown: 1
    }
  }]

  var delivery = deliveryList.filter(item => {
    return item.year == year && item.month == month
  })

  res.status(200)
    .send({delivery})
})

module.exports = router;
