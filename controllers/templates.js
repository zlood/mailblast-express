const {sequel} = require('sequelize');
const Template = require('./../db/models').Template;
const _ = require('lodash');

module.exports.createTemplate = (req, res, next) => {
  const template = Template.build({
    name: _.chain(req.body.name)
    .trim()
    .escape()
    .value(),
    mailTemplate: req.body.mailTemplate,
    templateType: _.escape(req.body.templateType)
  })
  .save()
  .then((template) => {
    res.status(200)
      .send(template);
  })
  .catch((e) => {
    res.status(400)
      .send(e);
  });
};

exports.getAllTemplate = (req, res, next) => {
  Template.findAll()
    .then((templates) => {
      if (!templates) return res.status(404).send('No template yet');

      res.render('template', {
        title: 'Template List',
        isCampaignActive: true,
        isTemplateListPage: true,
        templateData: templates
      });
    })
    .catch((e) => {
      res.render('template', {
        title: 'Template List',
        isCampaignActive: true,
        isTemplateListPage: true,
        error: e
      });
    });
};

exports.getTemplatebyID = (req, res, next) => {

};

exports.deleteTemplateByID = (req, res, next) => {
  const id = req.params.id;
  let result = [];
  
  Template.findById(id)
    .then((template) => {
      if (!template) return res.status(404).send('Object ID not found');

      result.push(template);
      template.destroy()
        .then((data) => {

          res.status(200)
            .send(`${result.length} row removed`);
        });
    })
    .catch((e) => {
      return res.status(400).send(e);
    });
};

exports.updateTemplateByID = (req, res, next) => {

};
